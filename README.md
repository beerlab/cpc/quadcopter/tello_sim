# tello_sim
THere is fork of [mascot package]()


## How run docker


Firstly, to enable gui run:

```
xhost +local:docker
```

Secondly, run suitable docker compose, for example:

```
docker compose -f docker-compose.local.yaml up
```

## Change drone model

By default, AR2 Parrot drone is set. To change the model add arg to roslaunch in docker compose file:

```
roslaunch tello_sim simulation.launch drone_name:=tello
```
To set up AR2 parrot back just delete arg init or write:

```
roslaunch tello_sim simulation.launch drone_name:=ar2_parrot
```

Below you can see comands to interact with simulation.

Only simulation.launch starts with docker. To run other commands you need to run them from another terminal in the same docker container.

#### Start the Simulation

```
roslaunch tello_sim simulation.launch
```

#### Take off drone (example for first drone)

```
rostopic pub /r1/drone/takeoff  std_msgs/Empty "{}" 

```
