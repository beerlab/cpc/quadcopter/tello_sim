ARG BASE_IMG

FROM ${BASE_IMG}

RUN dpkg --add-architecture i386 && \
    apt-get update && apt-get install -y --no-install-recommends \
        libxau6 libxau6:i386 \
        libxdmcp6 libxdmcp6:i386 \
        libxcb1 libxcb1:i386 \
        libxext6 libxext6:i386 \
        libx11-6 libx11-6:i386 && \
    rm -rf /var/lib/apt/lists/*

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
        ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
        ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics,compat32,utility

RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV LD_LIBRARY_PATH /usr/lib/x86_64-linux-gnu:/usr/lib/i386-linux-gnu${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}:/usr/local/nvidia/lib:/usr/local/nvidia/lib64

RUN apt-get update && apt-get install -y --no-install-recommends \
        pkg-config \
        libglvnd-dev libglvnd-dev:i386 \
        libgl1-mesa-dev libgl1-mesa-dev:i386 \
        libegl1-mesa-dev libegl1-mesa-dev:i386 \
        libgles2-mesa-dev libgles2-mesa-dev:i386 glmark2 && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update  && apt-get install -y python3-tk && pip3 install opencv-python PyYAML opencv-python
RUN apt-get update && apt-get upgrade -y && apt-get install -y libgazebo11-dev libtinyxml2-dev libignition-math6-dev \
        ros-noetic-gazebo-ros ros-noetic-gazebo-plugins ros-noetic-gazebo-ros-pkgs ros-noetic-gazebo-ros-control ros-noetic-pr2-gazebo-plugins &&\
    rm -rf /var/lib/apt/lists/*

COPY . /workspace/ros_ws/src/tello_sim

WORKDIR /workspace/ros_ws
RUN cd src/tello_sim && rosinstall . /opt/ros/noetic .
RUN /bin/bash -c "catkin build"

CMD ["/bin/bash -ci", "roslaunch tello_sim simulation.launch"]